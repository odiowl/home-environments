(defun bookmark-eww--make ()
  "Make eww bookmark record."
  `((filename . ,(plist-get eww-data :url))
    (title . ,(plist-get eww-data :title))
    (time . ,(current-time-string))
    (handler . ,#'bookmark-eww-handler)
    (defaults . (,(concat
                   ;; url without the https and path
                   (replace-regexp-in-string
                    "/.*" ""
                    (replace-regexp-in-string
                     "\\`https?://" ""
                     (plist-get eww-data :url)))
                   " - "
                   ;; page title
                   (replace-regexp-in-string
                    "\\` +\\| +\\'" ""
                    (replace-regexp-in-string
                     "[\n\t\r ]+" " "
		     (plist-get eww-data :title))))))))

(defun bookmark-eww-handler (bm)
  "Handler for eww bookmarks."
  (eww-browse-url (alist-get 'filename bm)))

(defun bookmark-eww--setup ()
  "Setup eww bookmark integration."
  (setq-local bookmark-make-record-function #'bookmark-eww--make))

;; Finally, add a hook to set the make-record-function
;;(add-hook 'eww-mode-hook 'od--eww-set-bookmark-handler)
(add-hook 'eww-mode-hook #'bookmark-eww--setup)

;; really do not use the internal bookmarks for eww?
;; (define-key eww-mode-map "b" #'bookmark-set)
;; (define-key eww-mode-map "B" #'bookmark-jump)

;; I am not sure if I really need or use it!
;; (require 'eww)

;; (defvar consult--source-eww
;;   (list
;;    :name     "Eww"
;;    :narrow   ?e
;;    :action   (lambda (bm)
;;                (eww-browse-url (get-text-property 0 'url bm)))
;;    :items    (lambda ()
;;                (eww-read-bookmarks)
;;                (mapcar (lambda (bm)
;;                          (propertize
;;                           (format "%s (%s)"
;;                                   (plist-get bm :url)
;;                                   (plist-get bm :title))
;;                           'url (plist-get bm :url)))
;;                        eww-bookmarks))))

;; (add-to-list 'consult-buffer-sources 'consult--source-eww 'append)

(defun od--consult-org-bookmarks-items ()
  (let* ((url (org-entry-get (point) "URL"))
	 (cdate (org-entry-get (point) "CREATED"))
	 (category (org-entry-get (point) "CATEGORY"))
	 (tags (delete "@link" (org-get-tags-at)))
	 (title (format "%s %s %s" (propertize cdate 'face 'font-lock-variable-name-face)
			;;(propertize (format "%s" category) 'face 'font-lock-keyword-face)
			(nth 4 (org-heading-components))
			(propertize (format "%s" (if (null tags) "" tags)) 'face 'font-lock-doc-face))))
    `(,title ,url)))

;; (od--consult-org-bookmarks-items)
