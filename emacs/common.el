
;; machine  == :host
;; login    == :user
;; password == :secret
;; port     == :port
(defun od--auth-get-field (host prop)
  "Find PROP in `auth-sources' for HOST entry."
  (let* ((source (auth-source-search :host host))
         (field (plist-get
                 (flatten-list source)
                 prop)))
    (if source
        field
      (user-error "No entry in auth sources"))))

;; define a function for getting a base64 encoded string for a
;; given username and password; this is useful in e.g. restcalls
;; with restclient
(defun od/base64-auth (username password)
  (format "Basic %s"
	  (base64-encode-string
	   (format "%s:%s" username password))))

(defun od--shell-sudo-cmd (cmd)
  "Send a CMD to the shell with sudo privileges."
  (let ((default-directory "/sudo::"))
    (async-shell-command cmd)))

(defun od--pm-update ()
  (interactive)
  (od--shell-sudo-cmd "dnf update"))

(defun od--pm-search (s)
  (interactive "sSearch: ")
  (async-shell-command (concat "dnf search " s)))

(defun od--flatpak-update ()
  (interactive)
  (async-shell-command "flatpak update"))
