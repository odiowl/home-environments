
(require 'dired)
(require 'dired-x)

;; don't show hidden files per default
(setq-default dired-omit-files-p t) ; Buffer-local variable
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

;; toggle omit-mode with C-x M-o
(add-hook 'dired-mode-hook #'dired-omit-mode)

;; Function for toggle hidden files
(defun dired-dotfiles-toggle ()
  "Show/hide dot-files"
  (interactive)
  (when (equal major-mode 'dired-mode)
    ;; if currently showing
    (if (or (not (boundp 'dired-dotfiles-show-p)) dired-dotfiles-show-p)
	(progn 
	  (set (make-local-variable 'dired-dotfiles-show-p) nil)
	  (dired-mark-files-regexp "^\\\.")
	  (dired-do-kill-lines))
      (progn (revert-buffer) ; otherwise just revert to re-show
	     (set (make-local-variable 'dired-dotfiles-show-p) t)))))

(defun dired-dotfiles-hide ()
  (interactive)
  (when (equal major-mode 'dired-mode)
    (progn
      (set (make-local-variable 'dired-dotfiles-show-p) nil)
      (dired-mark-files-regexp "^\\\.")
      (dired-do-kill-lines))))

;; define external image viewer
(setq image-dired-external-viewer "xdg-open")

;; keyboard shortcut for toggle hidden files
(define-key dired-mode-map (kbd ",") 'dired-dotfiles-toggle)

;; use defined program for extension with !
(setq dired-guess-shell-alist-user
      '(("\\.pdf\\'" "xdg-open")
	("\\.png\\'" "xdg-open")
	("\\.svg\\'" "xdg-open")
	("\\.jpg\\'" "xdg-open")))



