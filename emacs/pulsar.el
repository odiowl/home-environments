;; be sure to have clone the repo into this path
;; git clone https://gitlab.com/protesilaos/pulsar.git pulsar
(add-to-list 'load-path "~/.emacs.d/manual-packages/pulsar")

(require 'pulsar)
(pulsar-setup)

(customize-set-variable
 'pulsar-pulse-functions ; Read the doc string for why not `setq'
 '(recenter-top-bottom
   move-to-window-line-top-bottom
   reposition-window
   bookmark-jump
   other-window
   delete-window
   delete-other-windows
   forward-page
   backward-page
   scroll-up-command
   scroll-down-command
   windmove-right
   windmove-left
   windmove-up
   windmove-down
   windmove-swap-states-right
   windmove-swap-states-left
   windmove-swap-states-up
   windmove-swap-states-down
   tab-new
   tab-close
   tab-next
   org-next-visible-heading
   org-previous-visible-heading
   org-forward-heading-same-level
   org-backward-heading-same-level
   outline-backward-same-level
   outline-forward-same-level
   outline-next-visible-heading
   outline-previous-visible-heading
   outline-up-heading))

(setq pulsar-pulse t)
(setq pulsar-delay 0.055)
(setq pulsar-iterations 10)
(setq pulsar-face 'pulsar-generic)

;; pulsar does not define any key bindings.  This is just a sample that
;; respects the key binding conventions.  Evaluate:
;;
;;     (info "(elisp) Key Binding Conventions")
;;
;; The author uses C-x l for `pulsar-pulse-line' and C-x L for
;; `pulsar-highlight-line'.
(let ((map global-map))
  (define-key map (kbd "C-c h p") #'pulsar-pulse-line)
  (define-key map (kbd "C-c h h") #'pulsar-highlight-line))
