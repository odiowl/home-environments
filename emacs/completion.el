;; This file takes all configurations about my completion framework

;; Base configuration based on completions and minibuffer ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

;; Vertico ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package vertico
  :init
  (vertico-mode)

  ;; Grow and shrink the Vertico minibuffer
  (setq vertico-resize t)
  ;; set number of lines for completions
  (setq vertico-count 7))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; Orderless ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use the `orderless' completion style. Additionally enable
;; `partial-completion' for file path expansion. `partial-completion' is
;; important for wildcard support. Multiple files can be opened at once
;; with `find-file' if you enter a wildcard. You may also give the
;; `initials' completion style a try.
(use-package orderless
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

;; Marginalia ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Enable richer annotations using the Marginalia package
(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  :init
  (marginalia-mode))

;; Embark ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package embark
  :ensure t

  :bind (("C-." . embark-act)
	 ("M-." . embark-dwim)
	 ;; extend keymap for embark-file
	 :map embark-file-map
	 ("5" . find-file-other-frame)
	 ("s" . embark-collect-snapshot)
	 ("S" . od--sudo-find-file)))

(use-package consult
  :config
  (setq consult-narrow-key "<")
  (setq consult-preview-key (kbd "M-."))

  (defun buffer-list-for-mode (mode)
  (seq-filter (lambda (buffer)
                (eq mode (buffer-local-value 'major-mode buffer)))
              (buffer-list)))

  (defvar consult--source-vterm
    `(:name     "Terminal"
                :hidden   t
                :narrow   ?t
                :category buffer
                :state    ,#'consult--buffer-state
                :items    ,(lambda () (mapcar #'buffer-name (buffer-list-for-mode 'vterm-mode)))))

  (defvar consult--source-dired
    `(:name "Dired"
	    :hidden t
	    :narrow ?d
	    :category buffer
	    :state ,#'consult--buffer-state
	    :items ,(lambda () (mapcar #'buffer-name (buffer-list-for-mode 'dired-mode)))))

  (add-to-list 'consult-buffer-sources 'consult--source-vterm 'append)
  (add-to-list 'consult-buffer-sources 'consult--source-dired 'append))

(use-package embark-consult
  :ensure t
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; https://github.com/minad/corfu
(use-package corfu
  :custom
  (corfu-auto t)
  (corfu-cycle t)
  :init
  (global-corfu-mode))

(use-package dabbrev
  :bind (("M-/" . dabbrev-completion)
	 ("C-M-/" . dabbrev-expand)))

;; starting with Emacs 28
;; (use-package kind-icon
;;   :ensure t
;;   :after corfu
;;   :custom
;;   (kind-icon-default-face 'corfu-default)
;;   (kind-icon-use-icons t)
;;   :config
;;   (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;; Local Variables:
;; eval: (flycheck-mode -1)
;; End:
;;; config.el ends here
