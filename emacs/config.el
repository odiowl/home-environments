;; created: 2021-09-05
;; author: Oliver Dunkl
;;
;; This file is the base Emacs configuration where some basic configurations lives and loads
;; some other configuration files.
;;
;; 

;; Package management
(require 'package)

;; add Melpa to package-archives
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

;; Initialize packages, avoid reinitializing
(unless (bound-and-true-p package--initialized)
  (setq package-enable-at-startup nil)
  (package-initialize))

;; Install `use-package' if it isn't installed.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Configure `use-package'.
(eval-when-compile
  ;; show loading time for packages in *Message* buffer
  (setq use-package-verbose t)
  (setq use-package-always-ensure t))

(eval-when-compile
  (require 'use-package))

;; where to store backups of files and where will auto-saved stored
(setq backup-directory-alist '(("." . "~/.emacs.d/backups"))
      auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/\\1" t)))

;; Add base authinfo - File is only decrypted on my local machine, not in Repo
;; it is encrypted with git-crypt.
;; In this file I will only write some usernames and hostnames no passwords
;; will be stored there.
(add-to-list 'auth-sources "~/.environments/home/secrets/authinfo")

;; define base configuration directory for all emacs based configs
(setq emacs-base-config-directory "~/.environments/home/emacs")

;; define some world clocks, check with `display-time-world'
(setq display-time-world-list
      '(("Europe/Vienna" "Vienna - Local Time")
	("Europe/London" "London - UTC")
	("Asia/Jerusalem" "Jerusalem")))

;; don't popup warnings for native-comp only errors should be mentioned
(setq warning-minimum-level :error)

;; define my custom file and load stuff from there
;; this file will not be in VCS because it is only be generated
(setq custom-file (concat emacs-base-config-directory "/custom.el"))
(load custom-file)

;; IBuffer ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package ibuffer
  :ensure t

  :config
  ;; use human readable size column instead of original one
  (define-ibuffer-column size-h
    (:name "Size" :inline t)
    (cond
     ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
     ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
     ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
     (t (format "%8d" (buffer-size)))))

  ;; format columns for ibuffer
  (setq ibuffer-formats
	'((mark modified read-only " "
		(name 20 20 :left :elide) " "
		(size-h 9 -1 :right) " "
		(mode 16 16 :left :elide) " " filename-and-process)
	  (mark " " (name 16 -1) " " filename))
	;; don't show empty filter groups
	ibuffer-show-empty-filter-groups nil)

  ;; add hl-line-mode to ibuffer
  (add-hook 'ibuffer-mode-hook #'hl-line-mode)

  (setq ibuffer-saved-filter-groups
	'(("MAIN"
	   ("ORG" (mode . org-mode)))))

  :bind ("C-x C-b" . ibuffer))

;; Nix expressions/Nix mode
(use-package nix-mode
  :mode "\\.nix\\'")

;; don't ask loading risky local variables; it's because of using dir-locals.el in
;; some directories and with some risky local variables
(advice-add 'risky-local-variable-p :override #'ignore)

;; Loading other configurations ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; load common functions
(load-file (concat emacs-base-config-directory "/common.el"))

;; load configuration for gui elements such as toolbar, scrollbar, theme, ...
(load-file (concat emacs-base-config-directory "/gui.el"))

;; load completion frameworks
(load-file (concat emacs-base-config-directory "/completion.el"))

;; load comfigurations for dired
(load-file (concat emacs-base-config-directory "/dired.el"))

;; load org configurations
(use-package org
  :ensure org-contrib
  :config
  (use-package org-ql
    :ensure t
    ;; be sure to load right lib for org-ql
    ;; maybe obsolete in the future
    :init (load-file "~/.emacs.d/elpa/peg-1.0/peg.el"))
  (load-file (concat emacs-base-config-directory "/org.el")))

;; Load configuration for RSS Feed reader elfeed
(load-file (concat emacs-base-config-directory "/elfeed.el"))

;; Load configuration for additional functions for bookmarks
(load-file (concat emacs-base-config-directory "/bookmarks.el"))

;; Add git gutter
(use-package git-gutter
  ;; only useful for prog-mode?
  ;;:hook (prog-mode . git-gutter-mode)
  :config
  (setq git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  :config
  (define-fringe-bitmap 'git-gutter-fr:added [0] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [0] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [0] nil nil '(center repeated)))

;; Load major-mode specific configurations
(load-file (concat emacs-base-config-directory "/elisp.el"))

;; Load configuration for mu/mu4e
(load-file (concat emacs-base-config-directory "/mu4e.el"))

;; Load configuration for IRC
(load-file (concat emacs-base-config-directory "/erc.el"))

;; Load configuration for remote functions
(load-file (concat emacs-base-config-directory "/remote.el"))

;; Load functions for manipulating frames
(load-file (concat emacs-base-config-directory "/frame.el"))

;; load keyboard stuff, helpers and shortcuts
(load-file (concat emacs-base-config-directory "/keys.el"))

;; temporary load this file until it is in elpa
(load-file (concat emacs-base-config-directory "/pulsar.el"))

(load-file (concat emacs-base-config-directory "/lsp.el"))

(load-file "~/.environments/home/secrets/secrets.el")
