
(use-package eros)

(defun od--elisp-mode-configurations ()
  (display-line-numbers-mode t)
  (eros-mode t))

(add-hook 'emacs-lisp-mode-hook 'od--elisp-mode-configurations)
