;; mu4e.el
;; Author:  Oliver Dunkl
;; Created: 2017-07-21

;; add mu4e to the load-path
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

(require 'mu4e)
(require 'org-mu4e)

;; set function for sending emails and with sendmail-program
(setq message-send-mail-function 'message-send-mail-with-sendmail
      sendmail-program "msmtp")

;; Versuche den GPG Schlüssel vom Sender zu verwenden
(setq mml-secure-smime-sign-with-sender nil)
(setq mml-secure-openpgp-sign-with-sender nil)

;; Guide me through the sign/encrypt process
(setq mm-sign-option 'guided)
(setq mm-encrypt-option 'guided)

;; Only verify known protocols - default is 'never and I always get 'sign part undecided'
(setq mm-verify-option 'known)

;; Try to santize Re|RE|Aw|AW reply messages
(setq message-subject-re-regexp
      "^[ \t]*\\(\\([Rr][Ee]\\|[Aa][Ww]\\)\\(\\[[0-9]*\\]\\)*:[ \t]*\\)*[ \t]*")

;; Verwende shr HTML Renderer
(setq mml-text-html-renderer 'shr)
;; Hier weiß ich noch nicht was besser ist mit color oder ohne
;; das muß ich mir erst ansehen welche Auswirkungen das z.B. auf eww oder elfeed hat
(setq shr-color nil)
;; Damit kann man den Text etwas besser lesen bei HTML gerenderten Pages
(setq shr-color-visible-distance-min 5
      shr-color-visible-luminance-min 90)

;; my username
(setq user-full-name "Oliver Dunkl")

;; default mail directory
;; if we have the directory `sync-base-directory` use this
;; otherwise use ~/Mail
(setq mu4e-maildir "~/Mail")

;; link directly to current message
(setq org-mu4e-link-query-in-headers-mode t)

;; don't ask y/n for quit mu4e
(setq mu4e-confirm-quit t)

;; show always text/plain messages if available
;; if no text/plain is available use shr as HTML renderer
(setq mu4e-view-html-plaintext-ratio-heuristic most-positive-fixnum
      mu4e-html2text-command 'mu4e-shr2text)

;; work with invitations
(require 'mu4e-icalendar)
(mu4e-icalendar-setup)

;; delete message after reply
(setq mu4e-icalendar-trash-after-reply t)

;; do not autoupdate header view
(setq mu4e-headers-auto-update nil)

(defun mu4e-header-maildir-short (msg)
  "Show a shortname for Maildir in the header fields.
Use Maildir field from MSG."
  (let ((maildir (mu4e-message-field msg :maildir))
	(list (mu4e-message-field msg :mailing-list)))
    (cond
     ((string= maildir "/Private/Inbox") "/P.../Inbox")
     ((string= maildir "/Private/Archive") "/P.../Archive")
     ((string= maildir "/Private/Sent") "/P.../Sent")
     ((string= maildir "/Private/Muted") "/Muted")     
     ((string= maildir "/Work/Inbox") "/W.../Inbox")
     ((string= maildir "/Work/Archive") "/W.../Archive")
     ((string= maildir "/Work/Sent") "/W.../Sent")
     ((string= maildir "/Private/Mailinglists") "/Lists")
     ((string= maildir "/Private/Trash") "/P.../Trash")
     ((string= maildir "/Work/Trash") "/W.../Trash")
     ((string= maildir "/Private/Spam") "/Spam")
     (t maildir))))

;; Add short version of maildir to my custom header list.
(add-to-list 'mu4e-header-info-custom
  '(:maildir-short .
     (:name "Shortname for Maildir"
      :shortname "MDir"
      :help "Shortname for Maildirs"
      :function mu4e-header-maildir-short)))

;; configuration for headers in mu4e-headers-view
(setq mu4e-headers-sort-field :date
      mu4e-headers-sort-direction 'ascending
      mu4e-headers-include-related nil
      mu4e-headers-skip-duplicates t
      mu4e-headers-fields '((:human-date . 10)
			    ;;(:maildir-short . 15)
			    (:flags . 4)
			    ;;(:mailing-list . 13)
			    (:from . 20)
			    (:thread-subject))
      mu4e-headers-date-format "%F"
      mu4e-headers-time-format "%I:%M %p")

;; Man kann in der mu4e-headers-view unicode Character als Flags darstellen.
;; Ich verwende lieber ASCII-Zeichen dafür.
(setq mu4e-use-fancy-chars nil)

;; don't ask but execute when leaving headers view
(setq mu4e-headers-leave-behavior 'ask)

;; only ask for directory for saving multiple attachments
(setq mu4e-save-multiple-attachments-without-asking t)

;; don't split the window for viewing a message
(setq mu4e-split-view 'horizontal)
(setq mu4e-headers-visible-lines 7)
;;(setq mu4e-split-view 'single-window)
;;(setq mu4e-split-view nil)
;; for testing split vertical
;; (setq mu4e-split-view 'vertical)
;; (setq mu4e-headers-visible-columns (/ (frame-width) 2))

(defun mu4e-set-headers-visible-columns ()
  "Define width of headers view in vertical split view. Used in `mu4e-headers-mode-hook'
to set it regularly on different screen sizes."
  (setq mu4e-headers-visible-columns (/ (frame-width) 2)))

;; set width of headers view
(add-hook 'mu4e-headers-mode-hook 'mu4e-set-headers-visible-columns)

;; from-or-to prefix
(setq mu4e-headers-from-or-to-prefix '("" . "-> "))

;; wrap lines in mu4e-view-mode
(add-hook 'mu4e-view-mode-hook 'turn-on-visual-line-mode)

;; kill all message buffers on exit
(setq message-kill-buffer-on-exit t)

;; works better with mbsync
(setq mu4e-change-filenames-when-moving t)

;; define retriving message function and update interval
;; it is important to do this only on one computer
;; be sure that on all other computer we do only indexing and not syncing
(if (string= system-name "titan")
    ;; TODO: reactivate `scouts' for mbsync! currently there is an SSL-error there
    (setq mu4e-get-mail-command "mbsync work private gmail")
  (setq mu4e-get-mail-command "true"))

;; update interval for checking new messages
;; all 120s -> 2m; if `mu4e-get-mail-command` is set to "true" we only index our messages
;; instead of fetching new ones. If there is something else we execute this shell command.
(setq mu4e-update-interval nil)

;; performance improvements
(setq mu4e-index-cleanup nil     ;; don't do a full cleanup check
      mu4e-index-lazy-check t)   ;; don't consider up-to-date dirs

;; Hide all update messages
(setq mu4e-hide-index-messages t)

;; don't reply to myself
(setq mu4e-compose-dont-reply-to-self t)

;; complete addresses
;; https://www.djcbsoftware.nl/code/mu/mu4e/Address-autocompletion.html#Address-autocompletion
(setq mu4e-compose-complete-only-personal t)

;; define my mail-address in respect to the system environment
(setq mu4e-user-name-address-list
      `(,(od--auth-get-field "mu4e-priv-1" :user)
	,(od--auth-get-field "mu4e-work" :user)
	,(od--auth-get-field "mu4e-scouts" :user)
	,(od--auth-get-field "mu4e-priv-2" :user)))

;; Bookmarks die einerseits auf der Hauptseite ersichtlich sind mit einer
;; Übersicht von (ungelesenen/alle Nachrichten) und andererseits kann man die
;; Query mit der Taste 'b' und dem angegebenen Kürzel schnell abfragen.
(setq mu4e-bookmarks
      '((:name "Messages in all INBOXes" :key ?i
	       :query "maildir:/Private/Inbox or maildir:/Work/Inbox")
	(:name "Flagged Messages" :key ?f :query "flag:flagged")
	(:name "Mailinglist Kellerbuch" :key ?K
	       :query "flag:unread and to:kellerbuch@noreply.github.com and not flag:trashed")
	(:name "Mailinglist Emacs" :key ?E
	       :query "flag:unread and (list:emacs-devel.gnu.org or list:help-gnu-emacs.gnu.org) and not flag:trashed")
	(:name "Mailinglist Mu4e" :key ?M
	       :query "flag:unread and (to:mu@noreply.github.com or list:mu-discuss.googlegroups.com) and not flag:trashed")
	(:name "Mailinglist Clojure" :key ?C
	       :query "flag:unread and list:clojure.googlegroups.com and not flag:trashed")
	(:name "Metalab/C3W Mailinglist" :key ?L
	       :query "flag:unread and (list:public.lists.c3w.at or list:metalab.lists.metalab.at) and not flag:trashed")
	(:name "Messages in Mailinglists" :key ?l
	       :query "maildir:/Private/Mailinglists")
	(:name "Monitoring" :key ?T
	       :query "(maildir:/Work/Monitoring or from:root@localhost.localdomain) and not flag:trashed")
	(:name "Notifications" :key ?N
	       :query "maildir:/Work/Notifications and not flag:trashed")
	(:name "Unread Messages" :key ?u
	       :query "flag:unread and not flag:trashed")
	(:name "Muted Messages" :key ?X :hide-unread t
	       :query "maildir:/Private/Muted")
	(:name "Today Messages (last 24 hours)" :key ?t :hide-unread t
	       :query "date:1d..now")
	(:name "Messages last Week" :key ?w :hide-unread t
	       :query "date:1w..now and not maildir:/Private/Muted")))

;; -- Contexts
(setq mu4e-contexts
      `(,(make-mu4e-context
	  :name "Private"
	  :match-func (lambda (msg)
			(when msg (string-match-p "^/Private" (mu4e-message-field msg :maildir))))
	  :vars `((user-mail-address . ,(od--auth-get-field "mu4e-priv-1" :user))
		  (mu4e-sent-folder . "/Private/Sent")
		  (mu4e-drafts-folder . "/Private/Drafts")
		  (mu4e-trash-folder . "/Private/Trash")))
	,(make-mu4e-context
	  :name "Work"
	  :match-func (lambda (msg)
			(when msg (string-match-p "^/Work" (mu4e-message-field msg :maildir))))
	  :vars `((user-mail-address . ,(od--auth-get-field "mu4e-work" :user))
		  (mu4e-sent-folder . "/Work/Sent")
		  (mu4e-drafts-folder . "/Work/Drafts")
		  (mu4e-trash-folder . "/Work/Trash")))
	,(make-mu4e-context
	  :name "Scouts"
	  :match-func (lambda (msg)
			(when msg (string-match-p "^/Scouts" (mu4e-message-field msg :maildir))))
	  :vars `((user-mail-address . ,(od--auth-get-field "mu4e-scouts" :user))
		  (mu4e-sent-folder . "/Scouts/Sent")
		  (mu4e-drafts-folder . "/Scouts/Drafts")
		  (mu4e-trash-folder . "/Scouts/Trash"))))
      mu4e-context-policy 'pick-first
      mu4e-compose-context-policy 'ask)

;; -- Maildir shortcuts
(setq mu4e-maildir-shortcuts
      '(("/Work/Inbox" . ?w)
	("/Private/Inbox" . ?i)
	("/Private/Mailinglists" . ?l)
	("/Private/Spam" . ?S)
	("/Private/Archive" . ?I)
	("/Work/Archive" . ?W)))

;; Use default completing function for jumping to other maildir
;; if ivy is installed and ivy-mode is set to t then it uses
;; ivy-completing-read.
(setq mu4e-completing-read-function 'completing-read)

;; Add logging to sauron

;; Diese Funktion schreibt eine Message zu dem Sauron-Logger.
;; Die Priorität muß auch angegeben werden. Alle Prioritäten über 2 werden im
;; Default-Log angezeigt.
(defun mu4e-log-to-sauron (prio msg)
  "Logs MSG to sauron with the priority PRIO if it is available."
  (when (fboundp 'sauron-add-event)
    (sauron-add-event 'mu4e prio msg)))

;; Zeige im Sauron-Log an wenn ein Update gemacht wird.
(add-hook 'mu4e-update-pre-hook
	  (lambda () (mu4e-log-to-sauron 2 "Fetching messages and updating index")))

;; -- Refiling
(defun mu4e-refile-folder-function (msg)
  "Function for refiling `msg` into proper maildir."
  (let ((maildir (mu4e-message-field msg :maildir)))
    (cond ((string-match "/Scouts/Inbox" maildir) "/Scouts/Archive")
	  ((string-match "/Work/Inbox" maildir) "/Work/Archive")
	  ((string-match "/POI/Inbox" maildir) "/POI/Archive")
	  (t "/Private/Archive"))))

(setq mu4e-refile-folder 'mu4e-refile-folder-function)

;; -- Actions

;; function for showing patches inline
(defun od/mu4e-patch-view (msg)
  (let* ((body (mu4e-message-field msg :body-txt))
         (id (mu4e-message-field msg :message-id))
         (subject (mu4e-message-field msg :subject))
         (buffer (get-buffer-create (concat "*mu4e-patch-" id "*"))))
    (switch-to-buffer buffer)
    (erase-buffer)
    (insert subject)
    (insert "\n\n")
    (insert body))
    (set-buffer-modified-p nil)
    (diff-mode)
    (read-only-mode)
    (goto-char (point-min)))

(add-to-list 'mu4e-view-actions '("patch view" . od/mu4e-patch-view))

;; add action to view message as html in a browser
(add-to-list 'mu4e-view-actions
	     '("ViewInBrowser" . mu4e-action-view-in-browser) t)

;; edit message in message-mode
(defun mu4e-edit-raw-message (msg)
  (let ((path (mu4e-message-field msg :path))
	(docid (mu4e-message-field msg :docid))
	(maildir (mu4e-message-field msg :maildir))
	(flags (mu4e-message-field msg :flags)))
    (unless (and path (file-readable-p path))
      (mu4e-error "Not a readable file: %S" path))
    (find-file path)
    (message-mode)
    (local-set-key
     (kbd "C-c C-c")
     `(lambda ()
	(interactive)
	(save-buffer)
	(kill-buffer)
	(mu4e~proc-move ,docid ,maildir)))))

;; In mu4e-view-mode Key 'a' für actions und dann 'e' drücken um die
;; Message zu editieren.
(add-to-list 'mu4e-view-actions '("edit message" . mu4e-edit-raw-message))

;; -- Helper functions

;; count queries
(defun mu4e-count-query (query)
  (string-to-number (shell-command-to-string
		     (concat "mu find " (format "\"%s\"" query) " | wc -l"))))

;; Count queries and format result to a string.
;; If result is 0 then return "" otherwise return the number of the count as string.
(defun mu4e-count-query-wo-zero (query)
  (let ((nr (mu4e-count-query query)))
    (if (eq nr 0)
	""
      (format "%s" nr))))

(defun mu4e-print-last-query ()
  "Print the last query to status line."
  (interactive)
  (message "%s" mu4e~headers-last-query))

;; Messages/Threads muten
;; Vorlage gefunden hier:
;; https://emacs.stackexchange.com/questions/51999/muting-threads-in-mu4e
;; Ich hab das ganze etwas an meine Bedürfnisse angepasst.

;; Füge eine neue Mark hinzu. M für mute Message. Das heißt man kann dann einen
;; Thread mit 'T' oder 't' und mit 'M' markieren damit dieser in /Private/Mute
;; verschoben wird.
(add-to-list
 'mu4e-marks
 '(mute
   :char "M"
   :prompt "Mute"
   :show-target (lambda (target) "muted")
   :action (lambda (docid msg target)
	     (mu4e~proc-move docid "/Private/Muted" "+S-N-u"))))

;; Erstelle eine neue Funktion für muting in der headers-view und lege diese
;; Funktion auf die Taste 'M'.
(mu4e~headers-defun-mark-for mute)
(define-key mu4e-headers-mode-map (kbd "M") 'mu4e-headers-mark-for-mute)

;; Diese Funktion sucht alle Messages die das Tag 'muted' besitzen und holt sich
;; dazu alle related Messages. Danach werden alle Messages die sich noch nicht im
;; Folder /Private/Muted befinden dort hin verschoben.
(defun mu4e-move-muted-messages ()
  (let* ((cmd "mu find maildir:/Private/Muted --format=sexp -r")
	 (result (concat "(list " (shell-command-to-string cmd) ")"))
	 (msgs (car (read-from-string result))))
    (dolist (msg (cdr msgs))
      (let ((maildir (mu4e-message-field msg :maildir))
	    (docid (mu4e-message-field msg :docid))
	    (refloc "/Private/Muted"))
	(unless (or (string= refloc maildir)           ;; gleiches Verzeichnis
		    (string= "/Private/Sent" maildir)  ;; gesendete Messages
		    (string= "/Work/Sent" maildir)
		    (string= "/Private/Trash" maildir) ;; Messages im Papierkorb
		    (string= "/Work/Trash" maildir))
	  (when (and docid (> docid 0))
	    ;; TODO: can we make the log with message-id and a function
	    ;; for calling mu4e-headers-search with this message-id?
	    (mu4e-log-to-sauron 2 (format "Muting message %s" docid))
	    (mu4e~proc-move docid refloc "+S-N")))))))

;; Die Funktion `mu4e-move-muted-messages' soll automatisch ausgeführt werden
;; und alle Messages die ich nicht mehr sehen mag ein den Folder /Private/Muted
;; verschieben.
(add-to-list 'mu4e-index-updated-hook 'mu4e-move-muted-messages)

;; Funktion um all alten Messages im Verzeichnis /Private/Muted zu löschen.
;; Diese Messages sind muted damit ich nicht alle Thread lesen muß.
(defun mu4e-delete-old-muted-messages ()
  "Function for deleting old messages which are muted (in folder /Private/Muted).
We delete only messages which are older than 3 month."
  (let* ((query "maildir:/Private/Muted and date:..3m")
	 (nr (mu4e-count-query query)))
    (when (< 0 nr)
      (progn
	(mu4e-log-to-sauron 2 (format "Delete %s muted messages" nr))
	(call-process-shell-command (format "mu find --exec='rm' %s" query))))))

;; Funktion um gelöscht markierte Messages zu löschen. Diese werden nach
;; 2 Monaten von der Festplatte gelöscht.
(defun mu4e-delete-old-trashed-messages ()
  "Function for deleting old messages which are already deleted (marked as 'trashed').
We delete only messages which are older than 2 month."
  (let* ((query "flag:t date:..2m")
	 (nr (mu4e-count-query query)))
    (when (< 0 nr)
      (progn
	(mu4e-log-to-sauron 2 (format "Delete %s trashed messages" nr))
	(call-process-shell-command (format "mu find --exec='rm' %s" query))))))

;; Bevor neue Nachrichten abgeholt werden, werden die alten Nachrichten gelöscht.
(add-hook 'mu4e-update-pre-hook 'mu4e-delete-old-muted-messages)
(add-hook 'mu4e-update-pre-hook 'mu4e-delete-old-trashed-messages)

;; Einstellungen um die Anzahl von ungelesenen Nachrichten in der Mode-Line
;; anzuzeigen.
(setq display-time-mail-query "(maildir:/Private/Inbox or maildir:/Work/Inbox) and flag:unread")

(setq display-time-mail-function
      (lambda ()
	(let ((cnt (mu4e-count-query display-time-mail-query)))
	  (if (= cnt 0)
	      (progn
		(setq display-time-mail-string "")
		nil)
	    (progn
	      (setq display-time-mail-string
		    (concat "#" (mu4e-count-query-wo-zero display-time-mail-query)))
	      cnt)))))

(setq display-time-use-mail-icon nil)
(setq display-time-mail-string
      (concat "#" (mu4e-count-query-wo-zero display-time-mail-query)))
(setq display-time-mail-face 'font-lock-keyword-face)

;; command for list all using mailinglists in the last year
(setq mu4e-query-list-command
      (concat "mu find -f v flag:list m:/Private/Mailinglists date:1y.. | "
	      "sed -e '/^$/d' | sort | uniq -c | sort -rn | awk -- '{print $2}'"))

;; function for narrow down to some mailinglists
(defun mu4e-headers-search-narrow-lists ()
  (interactive)
  (let* ((output (shell-command-to-string mu4e-query-list-command))
	 (lists (split-string output "\n" t))
	 (list (completing-read "[mu4e] Jump to List: " lists)))
    (mu4e-headers-search-narrow (concat "v:" list))
    (mu4e-headers-rerun-search)))

;; -- Key bindings
;; mu4e-headers-mode
(define-key mu4e-headers-mode-map (kbd "v") 'mu4e-headers-search-narrow-lists)
(define-key mu4e-headers-mode-map (kbd ". s") 'mu4e-print-last-query)

;; mu4e-main-mode
(define-key mu4e-main-mode-map (kbd "=") 'mu4e-update-index)
(define-key mu4e-main-mode-map (kbd ". s") 'mu4e-print-last-query)
;; really quit mu4e and its buffers and processes, we will be asked for quit
;; if `mu4e-confirm-quit` is t.
(define-key mu4e-main-mode-map (kbd "Q") 'mu4e-quit)
;; don't quit mu4e process instead change to previous buffer
(define-key mu4e-main-mode-map (kbd "q")
  (lambda ()
    (interactive)
    (switch-to-buffer (other-buffer (current-buffer) 1))))

;; mu4e-view-mode
(define-key mu4e-view-mode-map (kbd "L") 'org-store-link)
(define-key mu4e-view-mode-map (kbd "c") #'mu4e-view-save-url)
(define-key mu4e-view-mode-map (kbd "C-c |") #'mu4e-change-split-vertical-and-balance)

;; ;; visible columns for vertical split view
;; (setq mu4e-headers-visible-columns 104)

;; Function for change horizontal split view to vertical and
;; balance windows.
(defun mu4e-change-split-vertical-and-balance ()
  (interactive)
  (progn
    (transpose-frame)
    (balance-windows)))

;; use gnus-view by default
(setq mu4e-view-use-gnus t)
    
(defun od/mu4e-toggle-gnus ()
 "Toggle `mu4e-view-use-gnus'."
 (interactive)
 (setq mu4e-view-use-gnus (not mu4e-view-use-gnus)))

(with-eval-after-load "mm-decode"
  (add-to-list 'mm-discouraged-alternatives "text/html")
  (add-to-list 'mm-discouraged-alternatives "text/richtext"))

(defun mu4e-gather-urls ()
  (let (urls)
    (goto-char (point-min))
    (while (re-search-forward goto-address-url-regexp (point-max) t)
      (push (match-string-no-properties 0) urls))
    (reverse urls)))

(defun mu4e-browse-urls (&optional kill)
  (interactive "P")
  (let ((urls (mu4e-gather-urls))
	(prompt (if kill "Copy URL to kill ring: " "Browse URL: "))
	(fn (if kill #'kill-new #'browse-url)))
    (if urls
	(funcall fn (completing-read prompt urls nil nil nil nil (car urls)))
      (message "No URLs found."))))

;; Local Variables:
;; eval: (flycheck-mode -1)
;; End:
;;; mu4e.el ends here
