{ config, pkgs, ... }:

{
  # xdg.desktopEntries.emacs = {
  #   name = "Emacs";
  #   exec = "emacs %F";
  #   type = "Application";
  #   icon = "emacs";
  #   terminal = false;
  # };

  # xdg.desktopEntries.emacs-client = {
  #   name = "Emacs-Client";
  #   exec = "emacsclient -c %f";
  #   type = "Application";
  #   icon = "emacs";
  #   terminal = false;
  # };
  
  # xdg.desktopEntries.rocket-chat = {
  #   name = "Rocket.Chat";
  #   exec = "rocketchat-desktop --file-forwarding chat.rocket.RocketChat @@u %U @@";
  #   terminal = false;
  #   type = "Application";
  #   icon = "chat.rocket.RocketChat";
  #   mimeType = [ "x-scheme-handler/rocketchat" ];
  #   comment = "Official OSX, Windows, and Linux Desktop Clients for Rocket.Chat";
  #   extraConfig = ''
  #     StartupWMClass=Rocket.Chat
  #   '';
  # };

  xdg.desktopEntries.chromium-browser = {
    name = "Chromium";
    exec = "${pkgs.chromium}/bin/chromium %U";
    terminal = false;
    icon = "chromium";
    comment = "Access the Internet";
    mimeType = [
      "application/pdf"
      "application/rdf+xml"
      "application/rss+xml"
      "application/xhtml+xml"
      "application/xhtml_xml"
      "application/xml"
      "image/gif"
      "image/jpeg"
      "image/png"
      "image/webp"
      "text/html"
      "text/xml"
      "x-scheme-handler/http"
      "x-scheme-handler/https"
      "x-scheme-handler/webcal"
      "x-scheme-handler/mailto"
      "x-scheme-handler/about"
      "x-scheme-handler/unknown"
    ];
    categories = [ "Network" "WebBrowser" ];
    actions = {
      new-window = {
        name = "Chromium (New Window)";
        exec = "${pkgs.chromium}/bin/chromium";
      };
      new-incognito-window = {
        name = "Chromium (New Incognito Window)";
        exec = "${pkgs.chromium}/bin/chromium --incognito";
      };
    };
  };

  # This desktop-entry will override the default from $pkgs.signal-desktop
  # because I need enabling support for wayland in the electron app
  xdg.desktopEntries.signal-desktop = {
    name = "Signal";
    exec = "${pkgs.signal-desktop}/bin/signal-desktop --enable-features=UseOzonePlatform --ozone-platform=wayland --no-sandbox %U";
    terminal = false;
    icon = "signal-desktop";
    comment = "Private messaging from your desktop";
    mimeType = [ "x-scheme-handler/sgnl" "x-scheme-handler/signalcaptcha" ];
    categories = [ "Network" "InstantMessaging" "Chat" ];
    settings = {
      StartupWMClass = "Signal";
    };
  };
}
  
