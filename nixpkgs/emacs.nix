{ config, pkgs, ... }:

let
  emacsPackage = pkgs.emacsPgtk;
in
{
  home.packages = with pkgs; [
    # after mu update I did:
    # mu init --maildir=~/Mail --my-address=...
    # mu index
    mu
  ];
  
  programs.emacs = {
    enable = true;

    # TODO: finally I will use the default emacs with --with-native-compilation
    #       and --with-pgtk (pure GTK) and not using the overlay anymore
    #package = pkgs.emacsPgtkGcc;
    package = emacsPackage;

    extraPackages = epkgs: (with epkgs; [
      use-package
      magit
      vterm
      consult
      ripgrep
      nix-mode
      org-ql
      git-gutter
      git-gutter-fringe
      elfeed
      modus-themes
    ]);

    # TODO: where does this file goes?
    extraConfig = ''
      (add-to-list 'load-path "${pkgs.mu}/share/emacs/site-lisp/mu4e")
      (setq mu4e-mu-binary "${pkgs.mu}/bin/mu")

      (load-file "~/.environments/home/emacs/config.el")

      (let ((file "~/.environments/home/emacs/remote-hosts.el"))
        (when (file-exists-p file)
          (load-file file)))
    '';
  };

  services.emacs = {
    enable = true;
    client = {
      enable = true;
    };
  };

}
