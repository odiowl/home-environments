{
  description = "My Home Manager configuration";

  inputs = {
    home-manager.url = "github:nix-community/home-manager";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    emacs-overlay.inputs.nixpkgs.follows = "nixpkgs";
    nixgl.url = "github:guibou/nixGL";
  };

  outputs = { nixpkgs, home-manager, emacs-overlay, nixgl, ... }:
    let
      system = "x86_64-linux";
    in {
      homeConfigurations.dellXPS13 = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.${system};
        modules = [
          ./home.nix
          {
            nixpkgs.overlays = [ emacs-overlay.overlay nixgl.overlay ];
            home = {
              username = "odi";
              homeDirectory = "/home/odi";
              stateVersion = "22.11";
            };
          }
        ];
      };
    };
}
