{
  enable = true;

  settings = {
    global = {
      font = "Fira Sans 11";
      alignment = "left";
      markup = "full";
      format = "<b>%s</b>\n%b";
      history_length = 20;
      idle_threshold = 60;
      shrink = "no";
      origin = "top-right";
      offset = "10x10";
      width = 400;
      show_indicators = "no";
      show_age_threshold = 60;
      padding = 10;
      ignore_newline = "no";
      icon_position = "left";
      horizontal_padding = 22;
      dmenu = "dmenu -p dunst:";
      separator_color = "#aaaaaa";
      corner_radius = 4;
      word_wrap = "yes";
      follow = "keyboard";
      ellipsize = "end";
      always_run_script = true;
      frame_width = 3;
      transparency = 8;
      stack_duplicates = false;
      hide_duplicate_count = true;
      max_icon_size = 32;
      progress_bar_height = 6;
      progress_bar_frame_width = 1;
      progress_bar_max_width = 450;
    };

    urgency_normal = {
      background = "#dddddd";
      foreground = "#000000";
      frame_color = "#29aeff";
      timeout = 10;
    };

    urgency_low = {
      background = "#aaaaaa";
      foreground = "#000000";
      frame_color = "#999999";
      timeout = 4;
    };

    urgency_critical = {
      background = "#eeeeee";
      foreground = "#a60000";
      frame_color = "#a60000";
      timeout = 15;
    };

    nextcloud_sync_activity = {
      appname = "Nextcloud";
      summary = "Sync Activity";
      body = "*could not be synced*";
      skip_display = "yes";
    };
  };
  
}
