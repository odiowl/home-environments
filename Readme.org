* Build my environment

It is important to add all files to git repository before building the system with the
following command:

#+begin_src emacs-lisp :dir ./nixpkgs/ :results silent
(async-shell-command "home-manager --impure switch --flake .#dellXPS13")
#+end_src

* Update flake.lock

#+begin_src bash :dir ./nixpkgs/ :results silent
nix flake update
#+end_src
